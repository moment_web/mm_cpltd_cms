<?php
    /**
     * config.php
     *
     * Author: pixelcave
     *
     * Configuration file. It contains variables used in the template as well as the primary navigation array from which the navigation is created
     *
     */


    // 關閉錯誤提示
    ini_set("display_errors","on");
    error_reporting(E_ALL);

    //Set header with utf-8
    header("Content-Type:text/html; charset=utf-8");
    // Set Timezone with Asia
    date_default_timezone_set("Asia/Taipei");
    /* 上傳資料夾配置 */
    $mFolder = "./upload/";

    /* 頁面配置 */
    $mPageLogin = 'http://' . $_SERVER['HTTP_HOST'] . '/CPLtd_CMS/login.php';
    $mPageIndex = 'http://' . $_SERVER['HTTP_HOST'] . '/CPLtd_CMS/index.php';

    /* DB設定 */
    $mDBConfig = array(
        "Host"      => "localhost",
        "Database"  => "CPLtd_CMS",
        "Port"      => "3306",
        "Username"  => "iniu",
        "Password"  => "iniu",
        "Chartset"  => "UTF8" ,
        "Device"    => "mysql"
    );

    /* Log path 設定 */
    $mLogPath = $_SERVER['DOCUMENT_ROOT'] . '/CPLtd_CMS/log/';


    /* Class 設定 */

    include_once 'class/MM_PDO_Class.php';
    $mPDO = new MM_PDO_Class($mDBConfig);

    include_once 'class/MM_Tools_Class.php';
    $mTools = new MM_Tools_Class();

    include_once 'class/MM_Log_Class.php';
    $mLog = new MM_Log_Class($mLogPath);

    include_once 'class/MM_Narrow_Class.php';

    /* Template variables */
    $template = array(
        // Login 頁面標題   
        'name'          => 'iniu 後台管理系統',
        // Login 頁面版本
        'version'       => '1.0',
        // 作者
        'author'        => 'Mars',
        'robots'        => 'noindex, nofollow',
        // 後台Title
        'title'         => 'iniu CMS',
        'description'   => '',
        // true                     enable page preloader
        // false                    disable page preloader
        'page_preloader'=> true,
        // true                     enable main menu auto scrolling when opening a submenu
        // false                    disable main menu auto scrolling when opening a submenu
        'menu_scroll'   => true,
        // 'navbar-default'         for a light header
        // 'navbar-inverse'         for a dark header
        'header_navbar' => 'navbar-default',
        // ''                       empty for a static layout
        // 'navbar-fixed-top'       for a top fixed header / fixed sidebars
        // 'navbar-fixed-bottom'    for a bottom fixed header / fixed sidebars
        'header'        => '',
        // ''                                               for a full main and alternative sidebar hidden by default (> 991px)
        // 'sidebar-visible-lg'                             for a full main sidebar visible by default (> 991px)
        // 'sidebar-partial'                                for a partial main sidebar which opens on mouse hover, hidden by default (> 991px)
        // 'sidebar-partial sidebar-visible-lg'             for a partial main sidebar which opens on mouse hover, visible by default (> 991px)
        // 'sidebar-alt-visible-lg'                         for a full alternative sidebar visible by default (> 991px)
        // 'sidebar-alt-partial'                            for a partial alternative sidebar which opens on mouse hover, hidden by default (> 991px)
        // 'sidebar-alt-partial sidebar-alt-visible-lg'     for a partial alternative sidebar which opens on mouse hover, visible by default (> 991px)
        // 'sidebar-partial sidebar-alt-partial'            for both sidebars partial which open on mouse hover, hidden by default (> 991px)
        // 'sidebar-no-animations'                          add this as extra for disabling sidebar animations on large screens (> 991px) - Better performance with heavy pages!
        'sidebar'       => 'sidebar-partial sidebar-visible-lg sidebar-no-animations',
        // ''                       empty for a static footer
        // 'footer-fixed'           for a fixed footer
        'footer'       => '',
        // ''                       empty for default style
        // 'style-alt'              for an alternative main style (affects main page background as well as blocks style)
        'main_style'    => '',
        // 'night', 'amethyst', 'modern', 'autumn', 'flatie', 'spring', 'fancy', 'fire' or '' leave empty for the Default Blue theme
        'theme'         => '',
        // ''                       for default content in header
        // 'horizontal-menu'        for a horizontal menu in header
        // This option is just used for feature demostration and you can remove it if you like. You can keep or alter header's content in page_head.php
        'header_content'=> '',
        'active_page'   => basename($_SERVER['PHP_SELF'])
    );

    /* Primary navigation array (the primary navigation will be created automatically based on this array, up to 3 levels deep) */
    $primary_nav = array(
        array(
            'name'  => '後台管理',
            'icon'  => 'gi gi-user',
            'url'   => 'admin'      
        ),
        array(
            'name'  => '產品管理',
            'icon'  => 'fa fa-ellipsis-h',
            'url'   => 'banner'
        ),
        array(
            'name' => '系統設定',
            'url' => 'system',
            'icon' => 'fa fa-cogs'
        )
    );
?>