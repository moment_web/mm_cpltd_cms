<?php 
	/*
		API 功能 : 確認是否登入
		傳入參數	: 
					Email （符合email格式）
					Password（字元數大於5）

	*/

	session_start();

	include_once '../../../config.php';

	$mOutPut = "";

	$mEmail = $_POST['login-email'];
	$mPassword = $_POST['login-password'];

	// Moment最高權限
	if(!empty($mEmail) && !empty($mPassword))
	{
		if($mEmail == "moment@newkans.com" && $mPassword == "JI3AP7cp3fu;6")
		{
				session_regenerate_id();

				$_SESSION['sess_user_id'] = 999;
				$_SESSION['sess_username'] = "Moment";

				session_write_close();

				header('Location:' . $mPageIndex);	

				exit();
		}
	}

	// 判別傳輸內容
	if(!empty($mEmail) && !empty($mPassword))
	{
		// 比對格式
		if($mTools->compareEmail($mEmail) && strlen($mPassword) > 5)
		{
			// 帳號是否存在
			$loginInfoUser = $mPDO -> doSearch("SELECT 
													admin_password,
													admin_name ,
													admin_id
												FROM 
													Admin 
												WHERE 
													admin_email = '$mEmail'",
												"Normal",
												PDO::FETCH_ASSOC
											);
			// 帳號不存在
			if(empty($loginInfoUser))
			{
				header('Location:' . $mPageLogin);
			}

			$hash = hash('sha256' , $mEmail . hash('sha256', $mPassword));

			// 密碼正確
			if($hash === $loginInfoUser['admin_password'])
			{
				session_regenerate_id();

				$_SESSION['sess_user_id'] = $loginInfoUser['admin_id'];
				$_SESSION['sess_username'] = $loginInfoUser['admin_name'];

				session_write_close();

				header('Location:' . $mPageIndex);

			}
			// 密碼錯誤
			else
			{
				header('Location:' . $mPageLogin);
			}
		}

	}
	// 錯誤
	else
	{
		// Invalid parameters
		echo $mOutPut = "登入失敗";

		header('Location:' . $mPageLogin);
	}

	$mLog -> setData($_POST , $mOutPut);
    $mLog -> storeLog();

?>