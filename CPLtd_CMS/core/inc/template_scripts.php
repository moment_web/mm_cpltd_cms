<?php
/**
 * template_scripts.php
 *
 * Author: pixelcave
 *
 * All vital JS scripts are included here
 *
 */

?>

<!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file (Remove 'http:' if you have SSL) -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>!window.jQuery && document.write(decodeURI('%3Cscript src="./core/js/vendor/jquery-1.11.1.min.js"%3E%3C/script%3E'));</script>
<!-- Bootstrap.js, Jquery plugins and Custom JS code -->
<script src="./core/js/vendor/bootstrap.min.js"></script>
<script src="./core/js/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
<script src="./core/js/plugins.js"></script>
<script src="./core/js/app.js"></script>
<script src="./core/js/ckeditor/ckeditor.js"></script>
<script src="./core/js/ckeditor/adapters/jquery.js"></script>
<script src="./core/js/helpers/jquery.confirm.min.js"></script>
<!-- 提示訊息 If you don't know can see that http://fabien-d.github.io/alertify.js/ -->
<script src="./core/js/helpers/alertify.min.js"></script>
<!-- 側邊顯示JS If you don't know can see that http://bootstrap-growl.remabledesigns.com/ 說明網址 -->
<script src="./core/js/pages/bootstrap-growl.js"></script>
