<?php

	/**
	 * 
	 * @authors Mars (mars@iniu.com.tw)
	 * @date    2014-08-15 16:00:00
	 * @version v1.0
	 */


	/*
		使用方法


		# Database config
		$mDbConfig = array(
							"Host" 		=> '127.0.0.1'	,
							"Database" 	=> 'Test'		,
							"Port" 		=> '3306'		,
							"Username" 	=> 'root'		,
							"Password" 	=> 'aabb1122'	,
							"Chartset" 	=> 'UTF8'
							"Device"	=> 'mysql'
						);

		$mPDO = new MM_PDO_Class($mDbConfig);

	*/

	
	class MM_PDO_Class extends PDO
	{
		protected $mDbHost 		= '127.0.0.1';
		protected $mDbDatabase 	= '';
		protected $mDbUserName 	= '';
		protected $mDbPassWord 	= '';
		protected $mDbPort 		= '3306';
		protected $mDbDevice 	= 'mysql';
		protected $mDbCharset 	= 'UTF8';

		protected $mDNS = '';

		protected $mStmt = '';
		protected $mTable = '';


		public function __construct($_config = array())
		{
			$this -> config($_config);
			$this -> connect();
		}

		# 連線
		private function connect()
		{	
			$this -> mDNS = $this->mDbDevice . ":" . "host=" . $this->mDbHost . ";" . "dbname=" . $this->mDbDatabase . ";" . "charset=" . $this->mDbCharset . ";"; 

			try 
			{
				parent::__construct($this->mDNS , $this->mDbUserName , $this->mDbPassWord);	
			}
			catch (PDOException $e)
			{
				print_r($e->getMessage());
				print_r($e->getCode());
				print_r($e->getFile());
				print_r($e->getTraceAsString());
				echo "資料庫連線失敗";
				exit();
			}
		}

		# 連線設置
		private function config($_config = array())
		{
			$this -> mDbHost 	 = $_config['Host'];
			$this -> mDbDatabase = $_config['Database'];
			$this -> mDbUserName = $_config['Username'];
			$this -> mDbPassWord = $_config['Password'];
			$this -> mDbPort 	 = $_config['Port'];
			$this -> mDbDevice 	 = $_config['Device'];
			$this -> mDbCharset  = $_config['Chartset'];
		}

 		
		// ----- 基本操作 ----- //

		# 搜尋 
		public function doSearch($_sql = "" ,$_fetch = "" , $_fetchMode = "")
		{
			$this->mStmt = parent::prepare($_sql);
			
			try
			{
				$this->mStmt->execute();

				switch ($_fetch) 
				{
					case 'Normal':
						$result = $this->mStmt->fetch($_fetchMode);
						break;

					case 'Object':
						$result = $this->mStmt->fetchObject();
						break;

					case 'All':
						$result = $this->mStmt->fetchAll($_fetchMode);
						break;

					case 'Column':
						$result = $this->mStmt->fetchColumn();
						break;

					default:

						$result = $this->mStmt->fetch($_fetchMode);
						break;
				}
			}
			catch(PDOException $e)
			{
				$result = $e;
			}
			return $result;
		}

		// ----- 原生用法 ----- //
		# 設置 Sql 語句
		public function setQuery($_sql)
		{
			$this->mStmt = parent::prepare($_sql);	

			return $this -> getErrorInfo();
		}

		# 設置搜尋方式
		public function setFetchMode($_mode)
		{
			$this->mStmt->setFetchMode($_mode);

			return $this -> getErrorInfo();
		}

		# 綁定 Param
		public function setBindParam($_key , $_value )
		{
			$this->mStmt->bindParam($_key , $_value , $this->getType($_value));

			return $this -> getErrorInfo();
		}

		# 綁定 Value
		public function setBindValue($_key , $_value )
		{
			$this->mStmt->bindValue($_key , $_value , $this->getType($_value));

			return $this -> getErrorInfo();
		}

		# 送出
		public function action()
		{
			$this->mStmt->execute();

			// $this->mStmt->debugDumpParams();
			return $this -> getErrorInfo();
		}



		// ----- 特殊用法 參數設置區塊 ----- //


		/*
			# insert
					1.insert data	
							array(
								"user_name" => "AAA"	,
								"user_nick_name" => "N1"	,
								"user_phone" => "123"
							)

					2. setTable
					3. call insert function 

		*/


		# 設置 Table 名稱
		public function setTable($_table)
		{
			$this->mTable = $_table;
		}

		#新增
		public function insert($_data = array())
		{
			$sqlKey = '';
			$sqlValue = '';

			$keys = array_keys($_data);
			$last = end($keys);


			foreach ($_data as $key => $value) 
			{
				$sqlKey .= ($key == $last) ? $key : $key . ",";
				$sqlValue .= ($key == $last) ? ":$key" : ":$key"  . ",";
			}

			$this->mStmt = parent::prepare("INSERT INTO $this->mTable ($sqlKey) VALUES ($sqlValue)");	
			
			foreach ($_data as $key => $value) 
			{
				$this->mStmt->bindValue(":$key" , $value , $this->getType($value));
			}

			// $this->mStmt->debugDumpParams();

			try
			{
				$this->mStmt->execute();
				return parent::lastInsertId();
			}
			catch(PDOException $e)
			{
				// print_r($e->getMessage());
				// print_r($e->getCode());
				// print_r($e->getFile());
				// print_r($e->getTraceAsString());


				return false;

				exit();
			}
		}


		// ----- 私有方法 ----- //

		# 取得資料型態
		private function getType($_data)
		{
			$type = 0;

			if (is_int($_data) || ctype_digit($_data))
	        {
				$_data = intval($_data);
				$type = PDO::PARAM_INT;
	        } 
	        elseif ($_data === NULL) 
	        {
				$type = PDO::PARAM_NULL;
	        } 
	        else 
	        {
				$type = PDO::PARAM_STR;
	        }

	        return $type;
		}

		# 取得錯誤 
		private function getErrorInfo()
		{
			if (!$this->mStmt) 
			{
				return parent::errorInfo();
			}
			else
			{
				return true;
			}
		}


	}
?>