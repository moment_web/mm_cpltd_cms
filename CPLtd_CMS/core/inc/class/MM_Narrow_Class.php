<?php
/**
 * 
 * @authors Your Name (you@example.org)
 * @date    2014-09-04 00:08:34
 * @version $Id$
 */
/**
* 
	*/
	class MM_NARROW_CLASS
	{

	
		public $imgSrc  	  ='';
		public $articleFolder ='';
		public $imgName 	  ='';
		public $width   	  ='';
		public $height  	  ='';

		public function __construct($_mNWconfig = array())
		{	
			$this ->imgSrc 		  = $_mNWconfig['tmp'];
			$this ->articleFolder = $_mNWconfig['folder'];
			$this ->imgName 	  = $_mNWconfig['name'];
			$this ->width         = $_mNWconfig['width'];
			$this ->height 	      = $_mNWconfig['height'];
		}
		public function pathProccess()
		{

			$path = $this ->articleFolder.$this ->imgName;
			return $path;
		}
		public function extProccess()
		{
			$ext = substr(strrchr($this ->imgName, '.'), 1);#獲取副檔名
			return $ext;
		}
		public function fileType()
		{	
			// 用檔名切割判斷用副檔名
			$ext    = strtolower(substr(strrchr($this ->imgSrc, '.'), 1));

			switch ($ext) 
			{
				case 'jpeg':

					return imagecreatefromjpeg($this ->imgSrc);

				break;

				case 'jpg':

					return imagecreatefromjpeg($this ->imgSrc);

				break;

				// case 'png':

				// 	return imagecreatefrompng($this ->imgSrc);

				// break;
				
				// 尚在測試
				// case 'gif':

				// 	// Create a new image instance
				// 	$im = imagecreatetruecolor(100, 100);

				// 	// Make the background white
				// 	imagefilledrectangle($im, 0, 0, 99, 99, 0xFFFFFF);

				// 	// Draw a text string on the image
				// 	return imagestring($im, 3, 40, 20, 'GD Library', 0xFFBA00);

				// break;
				// // 尚在測試
				// case 'wbmp':

				// 		$im = imagecreatetruecolor(120, 20);
				// 		$text_color = imagecolorallocate($im, 233, 14, 91);

				// return imagestring($im, 1, 5, 5,  'A Simple Text String', $text_color);

				// break;
				
				default:

					return "file type error!";					

					break;
			}
		}
		public function imgProccess()
		{	
			//讀取來源圖檔
			$src = $this ->fileType();
			//取得來源圖檔長寬 
			$src_w = imagesx($src);    
			$src_h = imagesy($src);

			//新圖檔長寬
			$new_w = $this ->width;              			
			$new_h = $this ->height;

			//建立空白縮圖
			$thumb = imagecreatetruecolor($new_w, $new_h);    

			//空白縮圖的背景顏色，設定空白縮圖的背景，如不設定，背景預設為黑色
			$bg = imagecolorallocate($thumb,255,0,255);

			//將顏色填入縮圖     
			imagefilledrectangle($thumb,0,0,$src_w,$src_h,$bg);  

			//執行縮圖
			imagecopyresampled($thumb, $src, 0, 0, 0, 0, $new_w, $new_h, $src_w, $src_h);

			switch ($this ->extProccess()) 
			{
				case 'png':

							imagepng($thumb, $this ->pathProccess());

					break;

				case 'gif':

							imagegif($thumb, $this ->pathProccess());

					break;
					
				case 'wbmp':

							imagewbmp($thumb, $this ->pathProccess());

					break;

				case 'jpg':

							imagejpeg($thumb, $this ->pathProccess(),80);		

					break;
				
				default:
							// 此為jpg檔案縮圖函數畫質設定為80%
							imagejpeg($thumb, $this ->pathProccess(),80);		
					break;

			}
		}
		public function __destruct()
		{

		}
	}
			#-------縮圖處理區塊----start-----------------#
			
			#-------縮圖處理區塊------end-----------------#
		
?>