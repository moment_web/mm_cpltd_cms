<?php  
    /*
        使用方法

            $mData = (!empty($_POST)) ? $_POST : null;

            $mLog = new MM_Log_Class();
            $mLog -> setData($mData , $mData);
            $mLog -> storeLog();

    */

    class MM_Log_Class
    {
    	protected $mInputDataJson = array();
    	protected $mOutputDataJson = array();

    	protected $mLogArr = array();

    	protected $mDirName = "../log/";
    	protected $mFileName= "";
    	
    	public function __construct($_dirName)
    	{
            $this -> mDirName = $_dirName;
    	}

        public function setData($_inputArr , $_outputArr)
        {
            $this -> mInputDataJson = $_inputArr;
            $this -> mOutputDataJson = $_outputArr;
        }

    	public function getServerIp()
    	{
    		return $_SERVER['SERVER_ADDR'];
    	}
    	public function getClientIp()
    	{
    		return $_SERVER['REMOTE_ADDR'];
    	}

    	public function getTime()
    	{	
    		return date("Y-m-d H:i:s");
    	}

    	public function getResquestUri()
    	{
    		return @$_SERVER['SERVER_NAME'] . @$_SERVER['REQUEST_URI'];
    	}

    	public function getRequestMehtod()
    	{
    		return $_SERVER['REQUEST_METHOD'];
    	}

    	public function getRequestHeaders()
    	{
    		return apache_request_headers();
    	}

    	public function getResponseHeaders()
    	{
    		return apache_response_headers();
    	}

    	public function getStatusCode()
    	{
    		return http_response_code();
    	}

    	public function storeLog()
    	{
    		// 生成記錄 Json
			$this -> mLogArr =array(

    				"Request" => array(
    						"Data" 	=> $this -> mInputDataJson 	,
    						"Time" 	=> $this -> getTime() 	,
    						"IP" 	=> $this -> getClientIp(),
    						"URI"	=> $this -> getResquestUri(),
    						"Method"=> $this -> getRequestMehtod(),
    						"Header"=> $this -> getRequestHeaders()
    					),
    				"Response" => array(
    						"Data"	=> $this -> mOutputDataJson,
    						"Time"	=> $this -> getTime() ,
    						"Status Code" => $this -> getStatusCode(),
    						"IP"	=> $this -> getServerIp(),
    						"Header"=> $this -> getResponseHeaders()
    					)
    			);


			// 取得文件名稱
			$this -> mFileName = $this -> getFileNameToday();

			// 檢查資料夾是否存在
			if (!is_dir($this -> mDirName)) 
			{
				mkdir($this -> mDirName);
			}

			// 檢查檔案是否存在
			if(@!file_exists($this -> mDirName . $this -> mFileName))
			{
				fopen($this -> mDirName . $this -> mFileName , 'w+');
			}

			$fileString = file_get_contents($this -> mDirName . $this -> mFileName);

			if(strlen($fileString) == 0)
			{
				// 沒有內容
				$dataArr = array();
			}
			else
			{
				$dataArr = json_decode($fileString ,true);
			}

			array_push($dataArr, $this -> mLogArr);
			file_put_contents($this -> mDirName . $this -> mFileName, json_encode($dataArr));

    	}

    	private function getFileNameToday()
    	{
    		return "log_" . date("Ymd") . ".txt";
    	}
    }
?>