<?php
/**
 * 
 * @authors Your Name (you@example.org)
 * @date    2014-08-27 16:54:46
 * @version $Id$
 */

	class MM_MAIL_CLASS 
	{
		private $setServer		= "";
    	private $setPort		= "";
        protected $boundary 	= "";
        public $mailSendName 	= "";
		public $mailSendFrom 	= "";
    	public $mailTo 			= "";
    	public $mailUser 		= "";
    	public $mailSubject		= "";
    	public $mailMessage 	= "";
    	public $mailFile  		= "";
        public $mailFontArray   = "";
    	

	    public function __construct()
	    {
	    	$this ->setServer	 = ini_set("SMTP","SMTP.GMAIL.com");
    	 	$this ->setPort		 = ini_set("SMTP_PORT","25");
        	$this ->boundary 	 = uniqid("");
	        $this ->mailSendName = "";
			$this ->mailSendFrom = "";
	    	$this ->mailTo 		 = "";
	    	$this ->mailUser 	 = "";
	    	$this ->mailSubject	 = "";
	    	$this ->mailMessage  = "";
	    	$this ->mailFile 	 = array();
	        $this ->mailFontArray= array();
	    	
	    }

	    public function mailSendNameArea()
	    {
	    	$mailSendName = '=?UTF-8?B?'.base64_encode($this ->mailSendName).'?=';

	    	return $mailSendName;

	    }

	    public function mailSendAddressArea()
	    {
	    	$mailSendFrom = $this ->mailSendFrom;

	    	return $mailSendFrom;

	    }

	    public function mailToArea()
	    {
	    	$mailTo = $this ->mailTo;

	    	return $mailTo;
	    }

	    public function mailUserArea()
	    {
	    	$mailUser = '=?UTF-8?B?'.base64_encode($this ->mailUser).'?=';

	    	return $mailUser;
	    }

	    public function mailSubjectArea()
	    {
	    	$mailSubject = $this ->mailSubject.$this ->mailUserArea();

	    	return $mailSubject;
	    }

	    public function mailMessageTableArea()
	    {
	    	$mailview = $this ->mailFontArray;
			$mailview = json_encode($mailview);
			
			$buildKey   = "";
			$buildValue = "";
			#-------mail表格CSS&產生配置區域-start-----------------#
			#使用表單的值產生的表格
			
			$tableStyle = 'color:#FFFFFF;background-color:#1ECF2E;';
			$tableStyle .= 'padding-right:5px;text-align:center;border-radius:.25em;';
			$table      = '<table style='.$tableStyle.'>'.'<thead>'.'<tr>';
			$middle     = '</tr>'.'</thead>'.'<tr>';
			$tableEnd   = '</tr>'.'</table>';
			#-------mail表格CSS&產生配置區域---end-----------------#
			foreach (json_decode($mailview,true) as $key => $value) 
			{
				$buildKey .= ($value == end(json_decode($mailview,true))) ? '<th>'.$key.'</th>' : '<th>'.$key.'</th>';
				$buildValue .= ($value == end(json_decode($mailview,true))) ? '<td>'.$value.'</td>': '<td>'.$value.'</td>';
			}
			#-------table語句組合-start-----------------#
			$tableContent = $table.$buildKey.$middle.$buildValue.$tableEnd;
			#-------table語句組合---end-----------------#
			
			return $tableContent;
	    }

	    public function mailMessageArea()
	    {
	    	$mailMessage = '';
	    	if (!empty($this ->mailFile) && isset($this ->mailFile) && is_array($this ->mailFile)) 
			{
	    		$mailMessage .= '--'.$this ->boundary."\r\n"; 
				$mailMessage .= 'Content-type: text/html; charset=UTF-8'."\r\n";// 信件				
				$mailMessage .= 'Content-Transfer-Encoding: 7bit'."\r\n";
				$mailMessage .= $this ->mailMessage."\r\n";

				if (!empty($this ->mailFontArray) && $this ->mailFontArray != null && $this ->mailFontArray !="") 
				{
					$mailMessage .= $this ->mailMessageTableArea()."\r\n";
				}
				$mailMessage .= '--'.$this ->boundary.''."\r\n"; 
				foreach ($this ->mailFile as $key => $value) 
				{			
					$mimeType = mime_content_type($value['tmp_name']);
					(!$mimeType) ? $mimeType ='application/unknown' :  "echo 我都不知道了怎辦？？";
					$fileOpen = fopen($value['tmp_name'], "rb");
					$fileRead = fread($fileOpen, filesize($value['tmp_name']));
					fclose($fileOpen);
					$fileRead = chunk_split(base64_encode($fileRead));
					$fileName = basename($value['name']);
					$mailMessage .= '--'.$this ->boundary."\r\n"; 
					$mailMessage .= 'Content-type:'.$mimeType.'; name='.$fileName."\r\n";// 信件				
					$mailMessage .= 'Content-Transfer-Encoding: base64'."\r\n";
					$mailMessage .= 'Content-Disposition:attachment;filename='.$fileName."\r\n";
					$mailMessage .= $fileRead."\r\n";
					$mailMessage .= '--'.$this ->boundary."\r\n"; 
				}
			}
			else
				{
					$mailMessage .= $this ->mailMessage."\r\n";
					if (!empty($this ->mailFontArray) && $this ->mailFontArray != null && $this ->mailFontArray !="") 
					{
						$mailMessage .= $this ->mailMessageTableArea()."\r\n";
					}
				}
			
			return $mailMessage;
		}

		public function mailHeaderArea()
		{
			if (!empty($this ->mailFile) && is_array($this ->mailFile)) 
			{

				$mailHeader = 'MIME-Version: 1.0'."\r\n";
				$mailHeader .= 'Content-Type:multipart/mixed; boundary='.$this ->boundary.';charset="UTF-8"'.'\r\n'; //宣告分隔字串 
				$mailHeader .= 'X-Mailer: PHP/'.phpversion()."\r\n";
				$mailHeader .= 'From: '.$this ->mailSendNameArea().$this ->mailSendAddressArea()."\r\n";

				return $mailHeader;
			}
			else
				{
					$mailHeader = 'MIME-Version: 1.0'."\r\n";
					$mailHeader .= 'Content-type: text/html; boundary='.$this ->boundary.';charset="UTF-8"'.'\r\n'; //宣告分隔字串 
					$mailHeader .= 'X-Mailer: PHP/'.phpversion()."\r\n";
					$mailHeader .= 'From: '.$this ->mailSendNameArea().$this ->mailSendAddressArea()."\r\n";

					return $mailHeader;
				}
		
		}

		public function mailFinalProccess()
		{
			if (!empty($this ->mailSendFrom) && $this ->mailSendFrom !='') 
			{
				if (!empty($this ->mailUser) && $this ->mailUser !='') 
				{

					mail($this ->mailToArea(),$this ->mailSubjectArea(),$this ->mailMessageArea(),$this ->mailHeaderArea());
					
					return true;
				}
			}
		}

		public function __destruct()
		{
			
		}
	}	
?>