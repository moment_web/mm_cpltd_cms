<?php  

	/**
	 * 
	 * @authors Mars (mars@iniu.com.tw)
	 * @date    2014-08-15 16:00:00
	 * @version v1.0
	 *
	 *
	 * 雜項工具

	 		1. 比對 json 格式
	 		2. 比對 APi Key
	 		3. 產生 Session Token
	 		4. 取得 TimeStamp
	 		5. 
	 */



	class MM_Tools_Class 
	{
		
		public function __construct()
		{

		}

		# 比對 Json 是否符合格式
		public function compareJson($_data , $_method)
	    {
	    	$result = "";

	    	switch ($_method) 
	    	{
	    		case 'encode':
	    			json_encode($_data,true);
	    			$result = (json_last_error() == JSON_ERROR_NONE) ? json_encode($_data,true) : false;
	    			break;
	    		
	    		case 'decode':
	    			json_decode($_data,true);
	    			$result = (json_last_error() == JSON_ERROR_NONE) ? json_decode($_data,true) : false;
	    			break;
	    	}

	    	return $result;
	    }

	    # 比對 ApiKey
	    public function compareApiKey($_apiKey , $_compareKey)
	    {
	    	$resultArr = array_keys($_apiKey, $_compareKey);

	    	return (!empty($resultArr)) ? $resultArr[0] : false ;
	    }

	    # 產生 Session Token
	    public function makeSessionToken($_data1, $_data2, $_time)
		{
			$encrypted = base64_encode(sha1($_data1 . $_time, true) . $_time);
			$signature = "";
			if (function_exists('hash_hmac'))
			{
				$signature = base64_encode(hash_hmac("sha1", $encrypted, $_data2, true));
			}
			else
			{
				$blocksize = 250;
				$hashfunc = 'sha1';
				if (strlen($_data2) > $blocksize)
				{
					$_data2 = pack('H*', $hashfunc($_data2));
				}
				$_data2 = str_pad($_data2, $blocksize, chr(0x00));
				$ipad = str_repeat(chr(0x36), $blocksize);
				$opad = str_repeat(chr(0x5c), $blocksize);
				$hmac = pack('H*', $hashfunc(($_data2 ^ $opad) . pack('H*', $hashfunc(($_data2 ^ $ipad) . $_data1))));
				$signature = base64_encode($hmac);
				return $signature;
			}
		
			return $signature;
		}

		# 取得目前 TimeStamp
		public function getTimeStamp()
		{
			// date_default_timezone_set('Asia/Taipie');
			return strtotime(date("YmdHis"));
		}

		# 日期格式轉換 Timestamp -> Y年m月d日 H時i分m秒 x毫秒 , tag 參數範例：Y-m-d H:i:s x
		public function microtime_format($_tag, $_time)
		{
			list($usec, $sec) = explode(".", $_time);
			$date = date($_tag,$usec);
			return str_replace('x', $sec, $date);
		}
		
		# 我忘記功能了
		public function microtime_float()
		{
			list($usec, $sec) = explode(" ", microtime());
			return ((float)$usec + (float)$sec);
		}

		# 產生亂數大寫字串
		public function getRandomString($_length)
		{
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    		$randomString = '';
    		for ($i = 0; $i < 4; $i++) 
    		{
        		$randomString .= strtoupper( $characters[rand(0, strlen($characters) - 1)] );
    		}
    		return $randomString;
		}

		# Error Code
		public function getErrorCodeArr($_code)
		{
			# Error Code
			$mErrorCode = array(
									'0' => 0 , // Success
									'1' => 1 , // Invalid parameters
									'2' => 2 , // Missing required parameters
									'3' => 3 , // Invalid API key
									'4' => 4 , // Internal errors
									'5' => 5 , // Generic error
									'6' => 6 , // Resource not found
									'7' => 7 , // Access denied
									'1001' => 1001 , // Invalid account or password
									'3001' => 3001 , // invalid SMS token
									'3002' => 3002 , // phone number has been created
									'6001' => 6001   // apply fail
							);

			return json_encode(array( "code" => $mErrorCode[$_code]));
		}


		/* 正規表示 */
		public function comparePhone($_number)
		{
			return (preg_match("/09[0-9]{2}[0-9]{6}/", $_number)) ? true : false;
		}
		public function compareEmail($_email)
		{
			return (preg_match("/^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$/", $_email)) ? true : false;
		}
	}

?>