<?php 
    // 取得管理員帳號列表
    $systemList = $mPDO -> doSearch(
                                        "SELECT
                                            *
                                        FROM
                                            System",
                                        "Normal",
                                        PDO::FETCH_ASSOC
                                    );
?>


<!-- Social Widgets Header -->
<div class="content-header">
    <div class="header-section">
        <h1>
            <i class="fa fa-gears"></i><?php echo $mFunctionTitle; ?><br><small><?php echo $mFunctionContent; ?></small>
        </h1>
    </div>
</div>
<!-- END Social Widgets Header -->
<ul class="breadcrumb breadcrumb-top">
    <li><?php echo $template['name']; ?></li>
    <li><a href=""><?php echo $mFunctionTitle; ?></a></li>
</ul>
<!-- END Forms General Header -->

<div class="row">
    <div class="col-md-12">
        <!-- Basic Form Elements Block -->
        <div class="block">

            <!-- Basic Form Elements Content -->
            <form id="systemFrom" action="./index.php?function=<?php echo $mFunction; ?>&action=add" method="post" class="form-horizontal form-bordered">
                <fieldset>
                    <legend><i class="fa fa-angle-right"></i> 系統設定</legend>
                </fieldset>
                <div class="form-group">
                    <label class="col-md-3 control-label">網站Title</label>
                     <div class="col-md-9">
                        <input type="text" id="system-title" name="system-title" class="form-control" value="<?php echo $systemList['system_title']; ?>">
                    </div>
                </div>
                 <div class="form-group">
                    <label class="col-md-3 control-label">聯絡電話</label>
                     <div class="col-md-9">
                        <input type="phone" id="system-phone" name="system-phone" class="form-control" value="<?php echo $systemList['system_phone']; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">地址</label>
                     <div class="col-md-9">
                        <input type="text" id="system-address" name="system-address" class="form-control" value="<?php echo $systemList['system_address']; ?>">
                    </div>
                </div>
                <fieldset>
                    <legend><i class="fa fa-angle-right"></i> Email 設定</legend>
                </fieldset>
                <div class="form-group">
                    <label class="col-md-3 control-label">系統服務 Email</label>
                    <div class="col-md-9">
                        <input type="text" id="system-email" name="system-email" class="form-control" placeholder="Email" value="<?php echo $systemList['system_email']; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">系統 Email 名稱</label>
                    <div class="col-md-9">
                        <input type="text" id="system-email-name" name="system-email-name" class="form-control" value="<?php echo $systemList['system_email_name']; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">信件大標題</label>
                    <div class="col-md-9">
                        <input type="text" id="system-email-subject" name="system-email-subject" class="form-control" value="<?php echo $systemList['system_email_subject']; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">信件小標題</label>
                    <div class="col-md-9">
                        <input type="text" id="system-email-title" name="system-email-title" class="form-control" value="<?php echo $systemList['system_email_title']; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">信件內容</label>
                    <div class="col-md-9">
                        <!-- <textarea id="system-email-body" name="system-email-body" rows="10" class="form-control textarea-editor"></textarea> -->
                        <textarea id="system-email-body" name="system-email-body" rows="10" name="textarea-ckeditor" class="ckeditor">
                            <?php echo $systemList['system_email_body']; ?>
                        </textarea>
                    </div>
                </div>
                 <fieldset>
                    <legend><i class="fa fa-angle-right"></i> 首頁 YouTube 網址</legend>
                </fieldset>
                <div class="form-group">
                    <label class="col-md-3 control-label">網址1</label>
                    <div class="col-md-9">
                        <input type="text" id="system-youtube-1" name="system-youtube-1" class="form-control" value="<?php echo $systemList['system_youtube_1']; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">網址2</label>
                    <div class="col-md-9">
                        <input type="text" id="system-youtube-2" name="system-youtube-2" class="form-control" value="<?php echo $systemList['system_youtube_2']; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">網址3</label>
                    <div class="col-md-9">
                        <input type="text" id="system-youtube-3" name="system-youtube-3" class="form-control" value="<?php echo $systemList['system_youtube_3']; ?>">
                    </div>
                </div>

                <div class="form-group form-actions">
                    <div class="col-md-9 col-md-offset-3">
                        <button type="submit" class="btn btn-sm btn-primary">
                             儲存
                        </button>
                    </div>
                </div>
            </form>
            <!-- END Basic Form Elements Content -->
        </div>
        <!-- END Basic Form Elements Block -->
    </div>

</div>

<!-- 載入 Login 動作集 -->
<script src="./system/js/list.js"></script>
<script>$(function(){ mSystem.init(); });</script>