<?php 
	// 取得 Action 參數
	$mAction = (!empty($_GET['action'])) ? $_GET['action'] : null;

	// 設置模組基本資訊
	$mFunctionTitle = "系統設定";
	$mFunctionContent = "後台系統設定";

	// 跳轉功能頁面
	switch ($mAction) 
	{
		case 'list':
			include_once 'list.php';
			break;
		case 'add':
			include_once 'add.php';
			break;
		case 'modify':
			include_once 'modify.php';
			break;
		case 'delete':
			include_once 'delete.php';
			break;
		
		default:
			include_once 'list.php';
			break;
	}
?>