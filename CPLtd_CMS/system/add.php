<?php  
	/*
		功能 : 系統管理者新增
		傳入參數	: 
					Name (不為空)
					Email （符合email格式）
					Password（字元數大於5）

	*/


	# 輸出
	$mOutPut = "";

	$mEmail 	= (!empty($_POST['system-email'])) ? $_POST['system-email'] : null;
	$mTitle 	= (!empty($_POST['system-title'])) ? $_POST['system-title'] : null;
	$mPhone 	= (!empty($_POST['system-phone'])) ? $_POST['system-phone'] : null;
	$mAddress 	= (!empty($_POST['system-address'])) ? $_POST['system-address'] : null;
	$mEmailName 	= (!empty($_POST['system-email-name'])) ? $_POST['system-email-name'] : null;
	$mEmailTitle 	= (!empty($_POST['system-email-title'])) ? $_POST['system-email-title'] : null;
	$mEmailSubject 	= (!empty($_POST['system-email-subject'])) ? $_POST['system-email-subject'] : null;
	$mEmailBody 	= (!empty($_POST['system-email-body'])) ? $_POST['system-email-body'] : null;

	$mYoutube1 = (!empty($_POST['system-youtube-1'])) ? $_POST['system-youtube-1'] : null;
	$mYoutube2 = (!empty($_POST['system-youtube-2'])) ? $_POST['system-youtube-2'] : null;
	$mYoutube3 = (!empty($_POST['system-youtube-3'])) ? $_POST['system-youtube-3'] : null;



	$mPDO -> setQuery(
						"UPDATE 
							System
						SET
							system_email 				= :mEmail ,
							system_title 				= :mTitle ,
							system_phone 				= :mPhone ,
							system_address 				= :mAddress,
							system_email_name 			= :mEmailName,
							system_email_title 			= :mEmailTitle,
							system_email_subject 		= :mEmailSubject,
							system_email_body 			= :mEmailBody,
							system_youtube_1 			= :mYoutube1,
							system_youtube_2 			= :mYoutube2,
							system_youtube_3 			= :mYoutube3 ");

	$mPDO -> setBindParam(":mEmail" , $mEmail);
	$mPDO -> setBindParam(":mTitle" , $mTitle);
	$mPDO -> setBindParam(":mPhone" , $mPhone);
	$mPDO -> setBindParam(":mAddress" , $mAddress);
	$mPDO -> setBindParam(":mEmailName" , $mEmailName);
	$mPDO -> setBindParam(":mEmailTitle" , $mEmailTitle);
	$mPDO -> setBindParam(":mEmailSubject" , $mEmailSubject);
	$mPDO -> setBindParam(":mEmailBody" , $mEmailBody);
	$mPDO -> setBindParam(":mYoutube1" , $mYoutube1);
	$mPDO -> setBindParam(":mYoutube2" , $mYoutube2);
	$mPDO -> setBindParam(":mYoutube3" , $mYoutube3);

	$mPDO -> action();

	# Log 檔案記錄
	$mLog -> setData($_POST , $mOutPut);
    $mLog -> storeLog();

    echo "<script> location.replace('" . $mPageIndex . "?function=" . $mFunction .  "');</script>";


?>