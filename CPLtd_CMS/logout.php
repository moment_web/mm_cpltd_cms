<?php 
	session_start(); 
	session_destroy(); 

	include './core/inc/config.php';
	include './core/inc/template_start.php'; 

	// header('Location:' . $mPageLogin);
	echo '<script type="text/javascript">location.replace("' . $mPageLogin . '");</script>';
?>

<div class="preloader themed-background">
    <h1 class="push-top-bottom text-light text-center"><strong><?php echo $template['name'] ?></strong></h1>
    <div class="inner">
        <h3 class="text-light visible-lt-ie9 visible-lt-ie10"><strong>Loading..</strong></h3>
        <div class="preloader-spinner hidden-lt-ie9 hidden-lt-ie10"></div>
    </div>
</div>
