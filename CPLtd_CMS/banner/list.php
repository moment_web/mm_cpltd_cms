<?php  
    include_once './core/inc/config.php';

    // 搜尋動作以時間編排
    $sql         = "SELECT 
                           * 
                    FROM 
                           Banner 
                    ORDER BY 
                           banner_sort ";

    $sdb         =  $mPDO->doSearch($sql,"All",PDO::FETCH_ASSOC);
?>   
<!-- Page content -->
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="fa fa-table"></i><?php echo $mFunctionTitle; ?><br><small><?php echo $mFunctionContent; ?><br></small>
            </h1>     
        </div>
    </div>
   <ul class="breadcrumb breadcrumb-top">
        <li>Welcome</li>
        <li><a href = "./index.php?function=banner&action=list"><?php echo $mFunctionTitle; ?></a></li>
    </ul>
    <!-- END Datatables Header -->

    <!-- Draggable Blocks Row -->
   <!--  <div class="block-title">
        <button class="btn btn-info" onclick="javascript:location.href='./index.php?function=banner&action=insert'">
           <i class="fa fa-plus"></i> 橫幅新增
        </button>
    </div> -->
    <!-- 開啟可拖拉功能  -->
    <div class="row">    
        <div class="col-md-8">
            <div id="drag" class=" draggable-blocks ">
        <?php   
                foreach ($sdb as $key => $value) :
        ?>   
                <div  class="block col-sm-6" data-itemid="<?php echo $value['banner_id']?>">
                    <div class="block-title" >
                        <div  class="block-options " >
                            <?php echo $value['banner_title'] ?>
                            <!-- 屬於傳統配置選項 -->
                            <!-- <a href="./index.php?function=banner&action=modify&mod=<?php echo $value['banner_id'] ?>" class="btn btn-alt btn-xs btn-default" data-toggle="tooltip" title="更新資訊"><i class="fa fa-cog"></i></a> -->
                            <div class="pull-right">
                                <a href="./index.php?function=banner&action=delete&del=<?php echo $value['banner_id'];?>&img=<?php echo $value['banner_photo_url']?>" class="btn btn-alt btn-xs btn-default" data-toggle="tooltip" data-original-title="刪除"><i class="fa fa-times"></i></a>
                            </div>
                        </div>
                        <!-- <h2>圖片展示 NO.<?php echo  $value['banner_id'] ?> </h2> -->
                        <input type="hidden" id="val_sort" name="val_sort" value="<?php echo $value['banner_sort']?>">
                    </div>
                    <div class=" block-section text-center">
                    <!-- Just wrap your image with a link to your large image and the attribute data-toggle="lightbox-image" -->
                        <a href="<?php echo defineLagreImgFolder.$value['banner_photo_url']?> " data-toggle="lightbox-image" title="<?php echo $value['banner_link']?>">
                            <img src="<?php echo defineSmallImgFolder.$value['banner_photo_url']?>" alt="image" width="100%" height="100%">
                        </a>
                    </div>
                </div>
                <?php
                    endforeach;
                ?>
                </div>
        </div>
        <div class="block col-md-4">
            <form id="form-validation" action="./index.php?function=banner&action=method" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                <input type="hidden" name="action" value="insert">
                 <div class="form-group">
                    <label class="col-md-3 control-label" for="val_title">圖片標題</label>
                    <div class="col-md-9">
                        <input type="text" id="val_title" name="val_title" class="form-control" data-toggle="tooltip" title="" data-original-title="請輸入您的圖片標題！" value="">
                    </div>
                </div>
               <!--  <div class="form-group">
                    <label class="col-md-3 control-label" for="val_content">圖片內容</label>
                    <div class="col-md-9">
                        <input type="text" id="val_content" name="val_content" class="form-control" data-toggle="tooltip" title="" data-original-title="請輸入您的圖片標題！" value="">
                    </div>
                </div> -->
                <div class="form-group">
                    <label class="col-md-3 control-label" for="val_link">好站連結</label>
                    <div class="col-md-9">
                        <input type="text" id="val_title" name="val_link" class="form-control" data-toggle="tooltip" title="" data-original-title="請輸入您的好站連結！" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="file">封面圖片</label>
                    <div class="col-md-9">
                  
                    <input type="file" name="file" id="file"  accept="image/jpg,image/jpeg">
                    <code style="margin-top: 10px">圖片大小限制為 2 MB !</code>
                    </div>
                </div> 
                <div class="form-group form-actions">
                    <div class="col-md-9 col-md-offset-3">
                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> 新增</button>
                        <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> 清空</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<!-- 自定義樣式 -->
<style type="text/css">
    .alert-growl {
    background-color: rgb(0, 0, 0);
    background-color: rgba(0, 0, 0, 0.8);
    border-color: rgb(255, 255, 255);
    border-width: 3px;
    color: rgb(255, 255, 255);
}
    .alert-pink {
    background-color: rgb(255, 223, 255);
    border-color: rgb(194, 97, 135);
    color: rgb(230, 96, 160);
}

</style>
<script type="text/javascript" src="./banner/js/jquery.dragsort-0.5.2.min.js"></script>
<!-- 引入自創檔案驗證JS -->
<script src="./banner/js/archives_Limit_Rules.js"></script>
<!-- 引入驗證JS -->
<script src="./banner/js/formsValidation.js"></script>
<!-- 表單驗證顯示JS -->
<script>$(function(){ FormsValidation.init(); });</script>
<!-- 側邊顯示JS -->
<script src="./core/js/pages/bootstrap-growl.js"></script>
<script>function GetFile() {$("#file").click();}</script>
<script type="text/javascript">
$("#drag").dragsort({ dragSelector: "div", 
                                  dragBetween: true,
                                  dragEnd: saveOrder, 
                                  placeHolderTemplate: "<div class=' themed-background-dark col-sm-2 placeHolder'></div>"});

        function saveOrder() 
        {
            var id = $(".col-sm-4").map(function() { return $(this).data("itemid"); }).get();

            var postData = {};
             for (var i = 0; i < id.length; i++) {

                 postData['itemid'] = id ;
                 
             };
         
            var jsonOfPostObj = JSON.stringify(postData);

            $.ajax(
            {
                url: './banner/sort.php',
                type: 'POST',
                data: 
                    {
                        "data": jsonOfPostObj
                    },
                    success : function (_res)
                    {
                        if (JSON.stringify(status))
                        {   
                            self.setTimeout("history.go(0)",0);
                        };
                    }

            });
            
        };

</script>
