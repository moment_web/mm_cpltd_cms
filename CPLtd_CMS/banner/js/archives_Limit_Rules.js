<!-- http://bootstrap-growl.remabledesigns.com/ 說明網址 -->
    
    // File element
    $mFile = $('#file');
    $mFile.click(function(){
        $.growl({
            title: '<strong><i class="fa fa-exclamation-triangle fa-fw"></i>封面圖片注意事項：</strong>',
            message: '<br>檔案限制上傳類型為：JPG,JPEG</br>'
        },
        {
            element:'body',
            type: 'growl',
            allow_dismiss:'true',
            delay:'7000',
            animate: 
                    {
                        enter: 'animated lightSpeedIn',
                        exit: 'animated lightSpeedOut'
                    }   
        });
        $.growl({
            title: '<strong><i class="fa fa-exclamation-triangle fa-fw"></i>封面圖片注意事項：</strong>',
            message: '<br>圖片邊長請大於1080，<br>否則部分圖片呈現可能會失真！</br>'
        },
        {
            element:'body',
            type: 'growl',
            allow_dismiss:'true',
            delay:'9000',
            animate: 
                    {
                        enter: 'animated flipInY',
                        exit: 'animated flipOutX'
                    }   
        });
    }); 
    // 判斷檔案大小
    $mFile.change(function(event) {

           // 限制大小
           var limit = 1024 * 1024 * 2;
           // 當前檔案大小
           var size  = event.currentTarget.files[0].size;

            if (this.value != '') 
            {
                var ext = this.value.split('.').pop().toLowerCase();

                if($.inArray(ext, ['jpg','jpeg']) == -1) 
                {
                    $("button:submit").addClass('disabled');
                    var imgstring = "只能上傳：JPG,JPEG 格式！";
                        alertify.error(imgstring);
                    return false;
                }   
            }else{
                    $("button:submit").removeClass('disabled');
            }

            if (size > limit) {

                $("button:submit").addClass('disabled');
                  var imgstring = "圖片檔案太大請重新選擇！限制為2MB";
                      alertify.error(imgstring);
            }
            else {
                  $("button:submit").removeClass('disabled');
            }

    });