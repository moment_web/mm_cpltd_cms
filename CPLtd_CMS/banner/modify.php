<?php   
     include_once './core/inc/config.php';

    if (!empty($_GET['action']) && $_GET['action']=='modify' && !empty($_GET['mod']) && $_GET['mod']!='') 
    {   
      
        //搜尋動作     
        $sql  ="SELECT 
                      * 
                FROM 
                      Banner 
                WHERE 
                      banner_id = $_GET[mod]";

        $stmt  = $mPDO ->doSearch($sql,"All",PDO::FETCH_ASSOC);
        
   
            
?>

<!-- Page content -->
    <!-- Validation Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-warning_sign"></i><?php echo $mFunctionTitle; ?><br><small>更新資訊</small>
            </h1>
        </div>
    </div>
        <ul class="breadcrumb breadcrumb-top">
            <li>Welcome</li>
            <li>
            <a href="./index.php?function=banner&action=list"><?php echo $mFunctionTitle; ?></a>
            </li>
            <li>
            <a href="./index.php?function=banner&action=modify&mod=<?php echo $_GET['mod']?>">更新橫幅圖</a>
            </li>
        </ul>
    <!-- END Validation Header -->

    <div class="row">
            <div class="block col-md-12">
            <?php

                 foreach ($stmt as $bannerKey => $bannerVal) :
                    
            ?>
                <!-- Form Validation Example Content -->
                <form id="form-validation" action="./index.php?function=banner&action=method" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                    <input type="hidden" name="action" value="modify">
                <input type="hidden" name="mod" value="<?php echo $_GET['mod']?>">
                     <div class="form-group">
                        <label class="col-md-3 control-label" for="example-text-input">橫幅圖敘述</label>
                        <div class="col-md-9">
                            <input type="text" id="example-text-input" name="val_comment" class="form-control" data-toggle="tooltip" title="" data-original-title="有什麼錯誤嗎？" value="<?php echo $bannerVal['banner_comment'];?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-file-input">封面圖片</label>
                        <div class="col-md-9">
                        <input type="file" name="file" id="file" style="display: none">
                        <code style="margin-top: 10px">圖片大小限制為 2 MB !</code>
                            <div class="col-sm-4 block-section text-center">
                                    <!-- Just wrap your image with a div.gallery-image and.. -->
                                <div class="gallery-image"> 
                                         <!-- .. add your image.. -->
                                    <img src="<?php echo defineBannerFolder.$bannerVal['banner_photo_url']?>" alt="image" >
                                        <!-- .. along with a div.gallery-image-options which will contain your hover links! -->
                                    <div class="gallery-image-options">
                                    <!-- Link to your large image with the attribute data-toggle="lightbox-image" -->
                                    <a href="<?php echo defineSmallImgFolder.$bannerVal['banner_photo_url']?>" data-toggle="lightbox-image" class="gallery-link btn btn-sm btn-primary">
                                    <i class="fa fa-eye"></i> View
                                    </a>
                                    <?php 
                                        if (!empty($bannerVal['banner_photo_url']) && $bannerVal['banner_photo_url']!="" && $bannerVal['banner_photo_url']!= NULL) :
                                        
                                    ?>
                                    <input type="hidden" name="originalImg" value="<?php echo $bannerVal['banner_photo_url'];?>">
                                    <?php
                                        endif;
                                    ?>
                                    <a href="javascript:void(GetFile())" class="btn btn-sm btn-alt btn-primary" data-toggle="tooltip" title="上傳新圖片">
                                    <i class="fa fa-pencil"></i>
                                    </a>
                                    </div>
                                </div>                       
                            </div>  
                        </div>
                    </div>
                    <div class="form-group form-actions">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Submit</button>
                            <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                        </div>
                    </div> 
                    </div>
                     <?php 
                            endforeach;
                        } 
                    ?>
                </form>
                <!-- END Form Validation Example Content -->
            </div>
            <!-- END Validation Block -->
        </div>

<!-- 自定義樣式grow專用 -->
<style type="text/css">

    .alert-growl {
    background-color: rgb(0, 0, 0);
    background-color: rgba(0, 0, 0, 0.8);
    border-color: rgb(255, 255, 255);
    border-width: 3px;
    color: rgb(255, 255, 255);
}
    .alert-pink {
    background-color: rgb(255, 223, 255);
    border-color: rgb(194, 97, 135);
    color: rgb(230, 96, 160);
}

</style>
<!-- END Page Content -->

<!-- 引入自創檔案驗證JS -->
<script src="./banner/js/archives_Limit_Rules.js"></script>
<!-- 引入驗證JS -->
<script src="./banner/js/FormsValidation.js"></script>
<!-- 引入側邊JS -->
<script src="./core/js/pages/bootstrap-growl.js"></script>
<!-- 驗證顯示JS -->
<script>$(function(){ FormsValidation.init(); });</script>
<!-- open hidden of file -->
<script>function GetFile() {$("#file").click();}</script>