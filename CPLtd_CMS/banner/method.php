<?php 
	include_once './core/inc/config.php';

	#-------共用變數區塊區塊---start-----------------#
	
	#-------共用變數區塊--------end-----------------#

	# I'm example area,Don't because forget me that delete me.
		
	#-------初始設定區塊----start-----------------#   

	#-------初始設定區塊------end-----------------#
	
	#-------共用變數區塊區塊---start-----------------#
	
	// 資料表設定
	$mPDO ->setTable('Banner');
	
	// 圖片資料夾陣列設定
	$folderArray = array( 
						  defineBannerFolder,
	                      defineLagreImgFolder,
	                      defineMediumImgFolder,
	                      defineSmallImgFolder
	                    );
	
    // 圖片處理區塊
	if (!empty($_FILES) && $_FILES['file']['error'] == 0 && $_FILES['file']['size'] > 0) 
	{   
		//獲取副檔名
		$ext    = strtolower(substr(strrchr($_FILES['file']['name'], '.'), 1));

		// 圖片名稱處理
		if (!empty($_POST['val_date']) && $_POST['val_date'] != NULL && $_POST['val_date'] !='' ) 
		{
			$date = date('YmdHis',$_POST['val_date']);
		}
		elseif (!empty($_POST['val_update']) && $_POST['val_update'] != NULL && $_POST['val_update'] !='') 
			{
				$date = date('YmdHis',$_POST['val_update']);
			}
			else
				{
					$date = date('YmdHis');
				}

				// Get Image information I'ts Array	
				$checkSize  = getimagesize($_FILES['file']['tmp_name']);

				
				// 圖片名稱組合
				$bannerImg 	    = $date.".".$ext;	
				// 暫存檔移動至資料夾	
				move_uploaded_file($_FILES['file']['tmp_name'],defineBannerFolder.$bannerImg);

				// 上傳檔案寬度
				$tmpWidth  = $checkSize['0'];

				// 上傳檔案高度
				$tmpHeight = $checkSize['1'];

				#------Calculating Want Setting Size
				$largeSize  = 1080;
				$mediumSize = 720;
				$smallSize  = 480;

				// 計算公式 = 原本圖寬 || 高 / (原本圖寬 || 高 / 鎖定大小)
				// calculatingFormula = origina Width ||  origina Height / (origina Width ||  origina Height / lockSize)

				if ($tmpWidth > $tmpHeight) 
				{
					// echo "寬大於高";

					$calculatingLargeImgSizeHeight  = $largeSize;
					$calculatingmediumImgSizeHeight = $mediumSize;
					$calculatingSmallImgSizeHeight  = $smallSize;

					$calculatinglargeImgSizeWidth  = round($tmpWidth / ($tmpHeight / $calculatingLargeImgSizeHeight)) ;
					$calculatingMediumImgSizeWidth = round($tmpWidth / ($tmpHeight / $calculatingmediumImgSizeHeight));
					$calculatingSmallImgSizeWidth  = round($tmpWidth / ($tmpHeight / $calculatingSmallImgSizeHeight));	

				}
				elseif($tmpWidth < $tmpHeight) 
					{

						// echo "高大於寬";
						
						$calculatinglargeImgSizeWidth  = $largeSize;
						$calculatingMediumImgSizeWidth = $mediumSize;
						$calculatingSmallImgSizeWidth  = $smallSize;

						$calculatingLargeImgSizeHeight  = round($tmpHeight / ($tmpWidth / $calculatinglargeImgSizeWidth)) ;
						$calculatingmediumImgSizeHeight = round($tmpHeight / ($tmpWidth / $calculatingMediumImgSizeWidth));
						$calculatingSmallImgSizeHeight  = round($tmpHeight / ($tmpWidth / $calculatingSmallImgSizeWidth));	

					}
				elseif ($tmpWidth = $tmpHeight) 
					{
						// echo "寬高相同";

						$calculatingLargeImgSizeHeight  = $largeSize;
						$calculatingmediumImgSizeHeight = $mediumSize;
						$calculatingSmallImgSizeHeight  = $smallSize;

						$calculatinglargeImgSizeWidth  = round($tmpWidth / ($tmpHeight / $calculatingLargeImgSizeHeight)) ;
						$calculatingMediumImgSizeWidth = round($tmpWidth / ($tmpHeight / $calculatingmediumImgSizeHeight));
						$calculatingSmallImgSizeWidth  = round($tmpWidth / ($tmpHeight / $calculatingSmallImgSizeHeight));	

						
					}
			
				#-------圖片處理區塊---start-----------------#
				// 分別產生大中小尺寸
				// 範例
				//  array(
				// 		 'tmp'    => 輸入需處理的檔案路徑,
				// 		 'folder' => 輸入目標路徑,
				// 		 'name'	  => 輸入檔案名稱,
				// 		 'width'  => 輸入想要的寬度,
				// 		 'height' => 輸入想要的高度
				// 	 )
						 
				$allImgArrayforClass = array(
												"largeImgArray"  => array(
																			 'tmp'    => defineBannerFolder.$bannerImg,
																			 'folder' => defineLagreImgFolder,
																			 'name'	  => $bannerImg,
																			 'width'  => $calculatinglargeImgSizeWidth,
																			 'height' => $calculatingLargeImgSizeHeight,
																		 ),
												"mediumImgArray" => array(
																			 'tmp'    => defineBannerFolder.$bannerImg,
																			 'folder' => defineMediumImgFolder,
																			 'name'	  => $bannerImg,
																			 'width'  => $calculatingMediumImgSizeWidth,
																			 'height' => $calculatingmediumImgSizeHeight,
																		 ),
												"smallImgArray"  => array(
																			 'tmp'    => defineBannerFolder.$bannerImg,
																			 'folder' => defineSmallImgFolder,
																			 'name'	  => $bannerImg,
																			 'width'  => $calculatingSmallImgSizeWidth,
																			 'height' => $calculatingSmallImgSizeHeight,
																		 )
					               			);
				// 生產三種尺寸
				foreach ($allImgArrayforClass as $key => $value) 
				{
			    	$allImgArrayforClass = new MM_NARROW_CLASS($value) ;	
					$allImgArrayforClass ->imgProccess();
				}
		}

	#-------共用變數區塊--------end-----------------#

	#-------新增處理區塊------start-----------------#
	if (!empty($_POST['action']) && $_POST['action']=='insert')
	{	
		// 資料庫-新增
		if (!empty($_FILES) && $_FILES['file']['error'] == 0 && $_FILES['file']['size'] > 0) 
		{	
			$maxSort = $mPDO ->doSearch("SELECT 
												banner_sort 
				           			     FROM
							                  Banner","All",PDO::FETCH_ASSOC);
			$maxarray = array();
			foreach ($maxSort as $key => $value) 
			{
				
				$maxarray[] = $value['banner_sort'];
			}

			if(count($maxarray) >=1)
			{
				$anser = max($maxarray)+1;
			}
			else
			{
				$anser = 1;
			}
			


			$data = array(		
							'banner_photo_url'	    => $bannerImg,
							'banner_title' 	        => $_POST['val_title'],
							// 'banner_content' 	    => $_POST['val_content'],
							'banner_link' 	        => $_POST['val_link'],
							'banner_sort'			=> $anser
							
												  				  
						 );
				
			#-----資料庫動作區塊------start-----------------#
			$mPDO ->insert($data);
			#----資料庫動作區塊--------end-----------------#
						
			echo "<script type='text/javascript'>";
			echo "window.location.href='./index.php?function=banner&action=list'";
		    echo "</script>"; 
		}
	}
	#-------新增處理區塊--------end-----------------#
	
	#-------更新處理區塊----start-----------------#
	if (!empty($_POST['action']) && $_POST['action']=='modify') 
	{
	
		// 資料庫-修改-包括圖片
		if (!empty($_FILES) && $_FILES['file']['error'] == 0 && $_FILES['file']['size'] > 0) 
		{
						
			#-----更新資料庫動作區塊------start------------#
			$sql="UPDATE 
			   			Banner
		     	  SET  
					    banner_photo_url		= :banner_photo_url,
					    banner_title			= :banner_title,
					    -- banner_content			= :banner_content,
					    banner_link			    = :banner_link
					   
				  WHERE 
		    	  	    banner_id = $_POST[mod]";
			$mPDO ->setQuery($sql);
			$mPDO ->setBindParam(":banner_photo_url"	 ,$bannerImg);
			$mPDO ->setBindParam(":banner_title"         ,$_POST['val_title']);
			// $mPDO ->setBindParam(":banner_content"       ,$_POST['val_content']);
			$mPDO ->setBindParam(":banner_link"         ,$_POST['val_link']);

			$mPDO ->action();
			
			// 判斷圖片是否存在	
			if (!empty($_POST['originalImg']) && $_POST['originalImg'] !="" && $_POST['originalImg']!= NULL) 
            {	
				$originalImg = $_POST['originalImg'];
				foreach ($folderArray as $key => $value) 
				{
					// 最後動作確認檔案是否存在
					if (file_exists($value.$originalImg)) 
					{
						unlink($value.$originalImg);
					}
				}

			}
			#-------更新處理區塊------end-----------------#

			// 返回列表
			echo "<script type='text/javascript'>";
			echo "window.location.href='./index.php?function=banner&action=list'";
		    echo "</script>"; 
		}
	}

			#-------測試程式碼區塊---start-----------------#
			 // $date = date("Y-m-d H:i:s",$d);//將時間戳轉為時間格式
			// echo "<pre>".print_r($sql,true)."</pre>";
		    // echo "<pre>".print_r($_FILES,true)."</pre>";
	    	// echo "<pre>".print_r($_POST,true)."</pre>";

			#-------測試程式碼區塊----end-----------------#
?>