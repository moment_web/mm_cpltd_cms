<?php 

	// 圖片資料夾路徑
	define("defineRootFolder",$mFolder."banners/");
	define("defineBannerFolder",defineRootFolder."origina/");
	define("defineLagreImgFolder" ,defineRootFolder."large/");
	define("defineMediumImgFolder",defineRootFolder."medium/");
	define("defineSmallImgFolder" ,defineRootFolder."small/");

	// 取得 Action 參數
	$mAction = (!empty($_GET['action'])) ? $_GET['action'] : null;

	// 設置模組基本資訊
	$mFunctionTitle = "產品管理";
	$mFunctionContent = "後台管理系統";

	// 跳轉功能頁面
	switch ($mAction) 
	{
		case 'list':
			include_once 'list.php';
			break;
		case 'insert':
			include_once 'insert.php';
			break;
		case 'modify':
			include_once 'modify.php';
			break;
		case 'delete':
			include_once 'delete.php';
			break;
		case 'method':
			include_once 'method.php';
			break;
		default:
			include_once 'list.php';
			break;
	}

	// delete local all folder img function
	function delImg($_stauts)
    {
        // 圖片資料夾陣列設定
        if ($_stauts === 1) 
        {
        
            $folderArray = array(     
                                   defineBannerFolder,
                                   defineLagreImgFolder,
                                   defineMediumImgFolder,
                                   defineSmallImgFolder
                                );
            
            foreach ($folderArray as $delkey => $delvalue) 
            {
                foreach (glob("{$delvalue}/*") as $key => $value) 
                {
                     unlink($value);
                }   
            }

            return true;
        }
        else
            {
                return false;
            }
    }
    // 開始刪除圖片
    // delImg(1);
?>