<?php
	session_start(); 
	include './core/inc/config.php'; 
?>

<?php 

	if(!isset($_SESSION['sess_user_id']) || trim($_SESSION['sess_user_id']) == "")
	{
		// exit(header('Location:' . $mPageLogin);
		echo '<script type="text/javascript">location.replace("'.$mPageLogin.'");</script>';
	}

	// 取得目前管理員資訊
	$mAdminProfileArr = $mPDO -> doSearch("SELECT
												admin_name ,
												admin_email
											FROM
												Admin
											WHERE
												admin_id = '$_SESSION[sess_user_id]'",
											'Normal',
											PDO::FETCH_ASSOC);
?>


<?php include './core/inc/template_start.php'; ?>
<?php include './core/inc/page_head.php'; ?>

<!-- Remember to include excanvas for IE8 chart support -->
<!--[if IE 8]><script src="js/helpers/excanvas.min.js"></script><![endif]-->

<?php include './core/inc/template_scripts.php'; ?>
<!-- Page content -->
<div id="page-content">
	<?php 
		if(!empty($_GET['function']))
		{
			$mFunction = $_GET['function'];
			include_once './' . $_GET['function'] . "/index.php";
		}
	?>
</div>
<!-- END Page Content -->

<?php include './core/inc/page_footer.php'; ?>



<!-- Google Maps API + Gmaps Plugin, must be loaded in the page you would like to use maps (Remove 'http:' if you have SSL) -->
<!-- <script src="https://maps.googleapis.com/maps/api/js?senor=false"></script> -->
<!-- <script src="./core/js/helpers/gmaps.min.js"></script> -->

<?php include './core/inc/template_end.php'; ?>