<?php 
	// 取得管理員帳號列表
	$adminList = $mPDO -> doSearch(
									"SELECT
										admin_id ,
										admin_name,
										admin_email
									FROM
										Admin",
									"All",
									PDO::FETCH_ASSOC
								);

?>

<!-- Social Widgets Header -->
<div class="content-header">
    <div class="header-section">
        <h1>
            <i class="gi gi-group"></i><?php echo $mFunctionTitle; ?><br><small><?php echo $mFunctionContent; ?></small>
        </h1>
    </div>
</div>
<!-- END Social Widgets Header -->

<!-- Main Row -->
<div class="row">
    <div class="col-lg-8">
		<!-- Advanced Widgets with Users Row -->
		<div class="row">
			<!-- 卡片 -->
			<?php 
				if(!empty($adminList)):
					foreach ($adminList as $key => $value) :
			?>
		  	<div class="col-sm-6 col-lg-4">
	            <!-- Widget -->
	            <div class="widget">
	                <div class="widget-extra-full">
	                    <div class="widget-options">
	                        <a href="javascript:void(0)" id="deleteAdmin" admin-id="<?php echo $value['admin_id'];?>" class="btn btn-xs btn-default" data-toggle="tooltip" title="" data-original-title="刪除">
	                        	<i class="fa fa-times"></i>
	                        </a>
	                    </div>
	                    <!-- Easy Pie Charts (initialized in js/app.js -> uiInit()) -->
	                   	<div class="widget-extra text-center">
	                   		<a href="page_ready_user_profile.php">
                        		<img src="./core/img/placeholders/avatars/avatar2@2x.jpg" alt="avatar" class="widget-image img-circle " width="64" height="64">
                    		</a>
		                </div>
	                </div>
	                <div class="widget-extra themed-background text-center">
	                    <h3 class="widget-content-light">
	                        <strong><?php echo $value['admin_name']; ?></strong><br>
	                        <small><?php echo $value['admin_email'] ?></small>
	                    </h3>
	                </div>
	            </div>
	            <!-- END Widget -->
	        </div>
		    <?php  
					endforeach;
				endif;
		    ?>
		</div>
		<!-- END Advanced Widgets with Users Row -->
    </div>

    <div class="col-lg-4">
 		
        <!-- Add Contact Block -->
        <div class="block">
            <!-- Add Contact Title -->
            <div class="block-title">
                <h2><i class="fa fa-plus"></i> 新增管理者</h2>
            </div>
            <!-- END Add Contact Title -->

            <!-- Add Contact Content -->
            <form id="form-addAdmin" action="./index.php?function=<?php echo $mFunction; ?>&action=add" method="post" class="form-horizontal form-bordered">
                <div class="form-group">
                    <label class="col-xs-3 control-label" for="add-admin-email">Email</label>
                    <div class="col-xs-9">
                        <input type="email" id="add-admin-email" name="add-admin-email" class="form-control" >
                    </div>
                </div>
                 <div class="form-group">
                    <label class="col-xs-3 control-label" for="add-admin-name">名稱</label>
                    <div class="col-xs-9">
                        <input type="text" id="add-admin-name" name="add-admin-name" class="form-control" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-3 control-label" for="add-contact-phone">密碼</label>
                    <div class="col-xs-9">
                        <input type="password" id="add-admin-password" name="add-admin-password" class="form-control" placeholder="請輸入超過五個字元">
                    </div>
                </div>
               <!--  <div class="form-group">
                    <label class="col-md-3 control-label" for="example-select">權限設置</label>
                    <div class="col-md-9">
                        <select id="example-select" name="example-select" class="form-control" size="1">
                            <option value="0">Please select</option>
                            <option value="1">Option #1</option>
                            <option value="2">Option #2</option>
                            <option value="3">Option #3</option>
                        </select>
                    </div>
                </div> -->
                <div class="form-group form-actions">
                    <div class="col-xs-12 col-xs-offset-3">
                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> 新增</button>
                    </div>
                </div>
            </form>
            <!-- END Add Contact Content -->
        </div>
        <!-- END Add Contact Block -->
    </div>
</div>
<!-- END Main Row -->

<!-- 載入 Login 動作集 -->
<script src="./admin/js/add.js"></script>
<script src="./admin/js/delete.js"></script>
<script>$(function(){ addAdmin.init(); });</script>
<script>$(function(){ deleteAdmin.init(); });</script>
