<?php  
/*
		功能 : 系統管理者刪除
		傳入參數	: 
					admin id

		輸出參數 :
					刪除成功 : 200
					刪除失敗 : 500
					指令錯誤 : 400

	*/

	include_once '../core/inc/config.php';

	$mOutPut = "";

	$mId = $_POST['admin_id'];

	// 判別傳輸內容
	if(!empty($mId))
	{
		$mPDO -> setQuery("DELETE FROM
								Admin
							WHERE
								admin_id = :id;");

		$mPDO -> setBindParam(':id' , $mId);
		if($mPDO -> action())
		{
			$mOutPut = "刪除成功";
			echo "200";	
		}
		else
		{
			$mOutPut = "刪除失敗";
			echo "500";
		}
	}
	// 錯誤
	else
	{
		// Invalid parameters
		$mOutPut = "刪除指令錯誤";
		echo "400";
	}

	$mLog -> setData($_POST , $mOutPut);
    $mLog -> storeLog();


?>