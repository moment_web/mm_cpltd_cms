<?php  
/*
		功能 : 系統管理者新增
		傳入參數	: 
					Name (不為空)
					Email （符合email格式）
					Password（字元數大於5）

	*/

	include_once './core/inc/config.php';

	$mOutPut = "";

	$mName = $_POST['add-admin-name'];
	$mEmail = $_POST['add-admin-email'];
	$mPassword = $_POST['add-admin-password'];

	// 判別傳輸內容
	if($mTools->compareEmail($mEmail) && strlen($mPassword) > 5 && !empty($mName))
	{
		// 帳號是否存在
		$loginInfoUser = $mPDO -> doSearch("SELECT 
												admin_id
											FROM 
												Admin 
											WHERE 
												admin_email = '$mEmail'",
											"Normal",
											PDO::FETCH_ASSOC
										);
		// 帳號不存在 新增帳號
		if(empty($loginInfoUser))
		{
			$insertDataArr = array(
					"admin_name" 		=> $mName,
					"admin_email" 		=> $mEmail,
					"admin_password" 	=> hash("sha256", $mEmail . hash("sha256", $mPassword)),
					"admin_created_time"=> $mTools -> getTimestamp()
				);

			$mPDO -> setTable('Admin');
			$mPDO -> insert($insertDataArr);

			echo '<div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4>
                    	<i class="fa fa-check-circle"></i> 
                    	新增成功
                    </h4> 
                </div>';


            echo "<script> location.replace('" . $mPageIndex . "?function=" . $mFunction .  "');</script>";
		}
		else
		{
			$mOutPut = "Email帳號已經申請過";
			echo '
					<div class="alert alert-danger alert-dismissable">
                    	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    	<h4>
                    		<i class="fa fa-times-circle"></i> 
                    		新增失敗
                    	</h4> 
                    		Email帳號已經申請過！
                	</div>';	
            echo "<script> location.replace('" . $mPageIndex . "?function=" . $mFunction .  "');</script>";
		}

	}
	// 錯誤
	else
	{
		// Invalid parameters
		$mOutPut = "新增失敗";
		echo '<div class="alert alert-danger alert-dismissable">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4>
	            	<i class="fa fa-times-circle"></i> 
	            	新增失敗
	            </h4> 
	        </div>';	

		echo "<script> location.replace('" . $mPageIndex . "?function=" . $mFunction .  "');</script>";
	}

	$mLog -> setData($_POST , $mOutPut);
    $mLog -> storeLog();


?>