/*
 *  Document   : delete.js
 *  功能		   : 刪除會員
 */

var addAdmin = function() {

    return {
        init: function() {

        	$('a[id=deleteAdmin]').click(function(){
        		var id = $(this).attr('admin-id');

        		var request = $.ajax({
				  	url: "./admin/delete.php",
				  	type: "POST",
				  	data: { "admin_id" : id },
				  	dataType: "html"
				});
				 
				request.done(function( _returnCode ) {
					switch(_returnCode)
					{
						case 200 :
							break;
						case 400 :
							break;
						case 500 :
							break;

						default:
							break;
					}
					
					location.reload();
				});
				 
				request.fail(function( jqXHR, textStatus ) {
					// 刪除失敗
					location.reload();
				});
        	});
        }
    };
}();