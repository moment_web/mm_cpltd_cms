/*
 *  Document   : login.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Login page
 */

var addAdmin = function() {

    // Function for switching form views (login, reminder and register forms)
    var switchView = function(viewHide, viewShow, viewHash){
        viewHide.slideUp(250);
        viewShow.slideDown(250, function(){
            $('input').placeholder();
        });

        if ( viewHash ) {
            window.location = '#' + viewHash;
        } else {
            window.location = '#';
        }
    };

    return {
        init: function() {

            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* 登入表單驗證 validate文檔：http://jqueryvalidation.org/validate */
            $('#form-addAdmin').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    e.closest('.form-group').removeClass('has-success has-error');
                    e.closest('.help-block').remove();
                },
                rules: {
                    'add-admin-name':{
                        required: true
                    },
                    'add-admin-email': {
                        required: true,
                        email: true
                    },
                    'add-admin-password': {
                        required: true,
                        minlength: 5
                    }
                },
                messages: {
                    'add-admin-name': '請輸入名稱',
                    'add-admin-email': '請輸入您的 Email 帳號',
                    'add-admin-password': {
                        required: '請輸入您的密碼',
                        minlength: '密碼錯誤'
                    }
                },
                submitHandler: function(_form) {
                    _form.ajaxForm();
                }
            });
        }
    };
}();